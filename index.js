// create new database called hotel

//insert a single room with inserOne method

db.rooms.insertOne(
    {
        "name": "single",
        "accomodates": 2,
        "price": 1000,
        "Description": "A simple room with all the basic necessities",
        "rooms_available": 10,
        "isAvailable": false
    }
)
    db.rooms.find()

// insert multiple rooms with insertMany method (double, queen)

db.rooms.insertMany([
	{
		"name": "double",
		"accomodates": 3,
		"price": 2000,
		"description": "A room fit for a small family going on a vacation",
		"rooms_available": 5,
		"isAvailable": false
	},
	{
		"name": "queen",
		"accomodates": 4,
		"price": 4000,
		"description": "A room fit with a queen sized bed perfect for a simple getaway",
		"rooms_available": 15,
		"isAvailable": false
	}

])

// use the find method to search for a room with the name double

db.rooms.find({"name":"double"})

// use the updateOne method to queen rooms

db.rooms.updateOne(

{"name": "queen"},
{$set: {"rooms_available": 0}}
)

// use the deleteMany method to delete all rooms that have 0 rooms available

db.rooms.deleteMany({"rooms_available": 0})